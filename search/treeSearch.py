import random
import createGraph

class sNode:
    def __init__(self, node = None, parent=None):
        self.node = node
        self.parent = parent

    def printPath(self):
        print self.node
        if self.parent:
            self.parent.printPath()
        

def treeSearch(start, end):
    frontier = [sNode(start)]

    while True:
        if not frontier:
            return False       # no path found
        next = frontier.pop()
        print next.node
        if next.node == end:
            return next
        toadd = next.node.neighbors
        for node in toadd:
            frontier.insert(0,sNode(node, next))

def doSearch(G):
    start = random.choice(G.nodelist)
    end = random.choice(G.nodelist)
    print "Searching for a path from {0} to {1}".format(start, end)
    path = treeSearch(start,end)
    if path:
        print "The solution path is:"
        path.printPath()
    else:
        print "No path found"


if __name__ == "__main__":
    G1 = createGraph.randomGraph1(500,0.05)
    G2 = createGraph.randomGraph2(500,1000)
    doSearch(G1)
    doSearch(G2)
