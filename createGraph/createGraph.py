# create some different graphs

import copy
import random

class Node:
    def __init__(self, value=None, neighbors=None):
        self.value = value
        self.neighbors = neighbors if neighbors else []

    def setnbd(self, nbd):
        self.neighbors = nbd

    def nbdString(self):
        """ string representation of the set of neighbors """
        string_rep = "["
        for node in self.neighbors:
            string_rep += "{0}, ".format(str(node))
        return string_rep[:-2] + "]" if len(string_rep) > 1 else "[]"

    def __str__(self):
        return "Node(%d)" % self.value

class Graph:
    def __init__(self, nodelist = None):
        self.nodelist = nodelist if nodelist else []

    def tableRep(self):
        """ print a primitive table representation of the graph """
        for node in self.nodelist:
            print "{0}:{1}".format(str(node), node.nbdString())

    def nodeCount(self):
        return len(self.nodelist)

    def edgeCount(self):
        count = 0
        for node in self.nodelist:
            count += len(filter(lambda x: hash(x) > hash(node),
                                node.neighbors))
        return count

    def path_p(self, path):
        """ predicate for path being a path in this graph """
        # Since we presume object identity all we need to check
        # is that the head of the path is in the graph
        return path[0] in self.nodelist

def completeGraph(size):
    nodelist = map(lambda x: Node(x), range(0,size))
    map(lambda x:x.setnbd(filter(lambda z: z.value != x.value,nodelist)), 
        nodelist)
    return Graph(nodelist)
        

def coinThrow(p):
    """ throw a coin that has probability p of returning True. """
    return random.random() <= p

def randomGraph1(size, p):
    """ returns a random graph created by adding any edge with prob p. """
    nodelist = map(Node, range(0,size))
    for node in nodelist:
        previous = filter(lambda x: x.value < node.value 
                          and node in x.neighbors,
                          nodelist)
        next = filter(lambda x: coinThrow(p) 
                      and x.value > node.value, 
                      nodelist)
        node.neighbors = previous + next
    return Graph(nodelist)
 
def randomGraph2(size, numEdges):
    """ returns a random graph created by reapetedly adding numEdges random edges """
    nodelist = map(Node, range(0, size))
    num = 0
    while num < numEdges:
        nodeA = random.choice(nodelist)
        nodeB = random.choice(nodelist)
        if nodeA != nodeB and nodeB not in nodeA.neighbors:
            nodeA.neighbors.append(nodeB)
            nodeB.neighbors.append(nodeA)
            num += 1
    return Graph(nodelist)

if __name__ == "__main__":
    K4 = completeGraph(4)
    print "Table representation of K4:"
    K4.tableRep()
    print "K4 has {0} edges, and {1} nodes.".format(K4.edgeCount(), K4.nodeCount())
    R1 = randomGraph1(5,0.5)
    print "A small random graph where every edge exists with prob 0.5:"
    R1.tableRep()
    print "This graph has {0} edges, and {1} nodes.".format(R1.edgeCount(),
                                                            R1.nodeCount())
    R2 = randomGraph2(5,6)
    print "A small graph with 6 randomly chosen edges existing:"
    R2.tableRep()
    print "This graph has {0} edges, and {1} nodes.".format(R2.edgeCount(),
                                                            R2.nodeCount())
